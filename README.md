# CI/CD for MongoDB Realm App - MongoDB Day 2022
Showcasing how to setup CI/CD for Realm App Services using Gitlab CI/. The Realm App Use is based on this repo: 
https://github.com/voxic/mdb_day_nov_22


# Gitlab Variables Configuration
You will need to configure CI/CD variables in the Settings for the Gitlab Project.
```
REALM_PUBLIC_API_KEY="<Realm Public Key>"
REALM_PRIVATE_API_KEY="<Realm Private Key>"
ATLAS_PROJECT_ID_DEV="<Atlas Project Id>"
```

# Additional Resources
## Blog Article - Building a Multi-Environment Continuous Delivery Pipeline for MongoDB Atlas
https://www.mongodb.com/developer/products/atlas/building-multi-environment-continuous-delivery-pipeline-mongodb-atlas/

## Documentation on Realm CLI
https://www.mongodb.com/docs/atlas/app-services/cli/